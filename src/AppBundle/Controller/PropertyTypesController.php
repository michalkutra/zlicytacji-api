<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class PropertyTypesController extends ApiController
{
	private $propertyTypes = array(
		0 => array(
			"name" => "Działka rolna",
			"urlname" => "dzialka-rolna",
		),
		1 => array(
			"name" => "Działka letniskowa",
			"urlname" => "dzialka-letniskowa",
		),
		2 => array(
			"name" => "Działka budowlana",
			"urlname" => "dzialka-budowlana",
		),
		3 => array(
			"name" => "Działka - pozostałe",
			"urlname" => "dzialka-inne",
		),
		4 => array(
			"name" => "Dom",
			"urlname" => "dom",
		),
		5 => array(
			"name" => "Budynek gospodarczy",
			"urlname" => "budynek-gospodarczy",
		),
	
	);
	
	/**
	 * @Route("/property-types.json", name="propertyTypes")
	 */
	public function propertyTypesAction()
	{
		return $this->callAction($this->propertyTypes);
	}
}
