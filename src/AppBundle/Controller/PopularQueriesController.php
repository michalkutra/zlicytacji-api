<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class PopularQueriesController extends ApiController
{
	private $popularQueries = array(
		0 => array(
			"name" => "Działka rolna Poznań",
			"property_urlname" => "dzialka-rolna",
			"city_urlname" => "poznan",
		),
		1 => array(
			"name" => "Działka rolna Warszawa",
			"property_urlname" => "dzialka-rolna",
			"city_urlname" => "warszawa",
		),
		2 => array(
			"name" => "Działka rolna Nowy Dwór Mazowiecki",
			"property_urlname" => "dzialka-rolna",
			"city_urlname" => "nowy-dwor-mazowiecki",
		),
		3 => array(
			"name" => "Działka letniskowa Poznań",
			"property_urlname" => "dzialka-letniskowa",
			"city_urlname" => "poznan",
		),
		4 => array(
			"name" => "Działka budowlana Poznań",
			"property_urlname" => "dzialka-budowlana",
			"city_urlname" => "poznan",
		),
		5 => array(
			"name" => "Działka budowlana Kraków",
			"property_urlname" => "dzialka-budowlana",
			"city_urlname" => "krakow",
		),
		6 => array(
			"name" => "Działka budowlana Stara Miłosna",
			"property_urlname" => "dzialka-budowlana",
			"city_urlname" => "stara-milosna",
		),
		7 => array(
			"name" => "Działka rolna Hel",
			"property_urlname" => "dzialka-rolna",
			"city_urlname" => "hel",
		),
		8 => array(
			"name" => "Działka budowlana Gdańsk",
			"property_urlname" => "dzialka-budowlana",
			"city_urlname" => "gdansk",
		),
		9 => array(
			"name" => "Działka letniskowa Hel",
			"property_urlname" => "dzialka-letniskowa",
			"city_urlname" => "hel",
		),
		10 => array(
			"name" => "Działka budowlana Gdynia",
			"property_urlname" => "dzialka-budowlana",
			"city_urlname" => "gdynia",
		),
	);
	
	/**
	 * @Route("/popular-queries.json", name="popularQueries")
	 */
	public function popularQueriesAction()
	{
		return $this->callAction($this->popularQueries);
	}
}
