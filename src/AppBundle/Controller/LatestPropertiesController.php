<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class LatestPropertiesController extends ApiController
{
	private $latestProperties = array();
	
	/**
	 * @Route("/latest-properties.json", name="latestAuctions")
	 */
	public function propertyTypesAction()
	{
		$propertyObj = new PropertyController();
		$i = 0;
		
		while ($i++ < 8) {
			array_push($this->latestProperties, $propertyObj->getProperty());
		}
		
		return $this->callAction($this->latestProperties);
	}
}
