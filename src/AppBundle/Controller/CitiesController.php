<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class CitiesController extends ApiController
{
	private $cities = array(
		0 => array(
			"name" => "Warszawa",
			"urlname" => "warszawa",
		),
		1 => array(
			"name" => "Poznań",
			"urlname" => "poznan",
		),
		2 => array(
			"name" => "Kraków",
			"urlname" => "krakow",
		),
		3 => array(
			"name" => "Bydgoszcz",
			"urlname" => "bydgoszcz",
		),
		4 => array(
			"name" => "Gdynia",
			"urlname" => "gdynia",
		),
		5 => array(
			"name" => "Nowy Dwór Mazowiecki",
			"urlname" => "nowy-dwor-mazowiecki",
		),
		6 => array(
			"name" => "Stara Miłosna",
			"urlname" => "sara-milosna",
		),
		7 => array(
			"name" => "Sierpc",
			"urlname" => "sierpc",
		),
		8 => array(
			"name" => "Zakopane",
			"urlname" => "zakopane",
		),
		9 => array(
			"name" => "Hel",
			"urlname" => "hel",
		),
		10 => array(
			"name" => "Katowice",
			"urlname" => "katowice",
		),
	);
	
	/**
	 * @Route("/cities.json", name="cities")
	 */
	public function citiesAction()
	{
		return $this->callAction($this->cities);
	}
}
