<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class StatsController extends ApiController
{
	private $globalAuctionsCount = array(
		"count" => 12322,
	);
	
	/**
	 * @Route("/stats/global-auctions-count.json", name="globalAuctionsCoun")
	 */
	public function getGlobalAuctionsCount ()
	{
		return $this->callAction($this->globalAuctionsCount);
	}
}
