<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class ApiController extends Controller
{
	public function callAction($data)
	{
		$response = new Response();
		$response->setContent(json_encode($data));
		
		$response->headers->set('Content-Type', 'application/json');
		
		return $response;
	}
}
