<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class SearchListingController extends ApiController
{
	private $searchResults = array(
		"search_results" => array(),
		"current_page" => null,
		"all_results_count" => 86,
		"query" => array(
			"type" => array(
				"name" => "Działka letniskowa",
				"urlname" => "dzialka-letniskowa",
			),
			"city" => array(
				"name" => "Poznań",
				"urlname" => "poznan",
			),
		),
	);
	
	/**
	 * @Route("/search-results.json", name="searchResults")
	 *
	 * @param Request $request
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function searchListingAction(Request $request)
	{
		$propertyObj = new PropertyController();
		$i = 0;
		
		while($i++ < 20) {
			array_push($this->searchResults["search_results"],$propertyObj->getProperty());
		}
		
		$this->searchResults["current_page"] = $request->get('page', 1);
		$this->searchResults["query"]["type"]["urlname"] = $request->get('type_urlname');
		$this->searchResults["query"]["city"]["urlname"] = $request->get('city_urlname');
		
		return $this->callAction($this->searchResults);
	}
}
