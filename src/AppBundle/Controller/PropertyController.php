<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class PropertyController extends ApiController
{
	private $property = array(
		"id" => 123,
		"title" => "Działka o pow. 1356m2 w Jaroslawcu",
		"address" => array(
			"city" => "Warszawa",
			"street" => "Kolejowa 5/7",
			"postcode" => "01-217",
			"lat" => "52.25",
			"lon" => "25.52",
			"boundry_box" => "33.5362475,-111.9267386;33.5104882,-111.9627875;33.5004686,-111.9027061;",
		),
		"surface" => "4325",
		"mortgage_register" => "P1LP/386480/8",
		"auction_price" => "25000",
		"estimated_price" => "32000",
		"type" => "Działka letniskowa",
		"auction" => array(
			"date" => "2017-07-08 20:00:00",
			"created_at" => "2017-06-01 10:00",
			"is_second" => true,
			"address" => array(
				"city" => "Stara Milosna",
				"street" => "Ubartkanachacie 7",
				"postcode" => "01-234",
				"lat" => "52.25",
				"lon" => "25.52",
				"boundry_box" => "",
			)
		),
	);
	
	/**
	 *
	 */
	public function getProperty() {
		return $this->property;
	}
	
	/**
	 * @Route("/property.json", name="property")
	 */
	public function propertyTypesAction()
	{
		return $this->callAction($this->property);
	}
}
